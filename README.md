# fsm-slack-bot

A slack bot for spreading the word of the Flying Spaghetti Monster.

https://en.wikipedia.org/wiki/Flying_Spaghetti_Monster

## Development

### Initial Setup

| Instruction | Command  |
|---|---|
| Start by creating a venv | `make venv` |
| Activate that venv | `source .venv/bin/activate` |
| Install dependencies |`make install-deps` |

### Dev Cycle

In one terminal run

```
export SLACK_SIGNING_SECRET=***
export SLACK_BOT_TOKEN=xoxb-***
make run
```

In another terminal

```
ngrok http 3000
```

### All Other Commands

```
Usage:
    help:         Prints this screen
    venv:         Creates a virtual env
    install-deps: Installs dependencies
    check-fmt:    Checks the code for style issues
    fmt:          Make the formatting changes directly to the project
    lint:         Lints the code
    test:         Runs the local tests
    run:          Runs the slack bot
    clean:        Clean out temporaries
```

## Layout

```
.
├── app.py                - The slack bot
├── Makefile              - A proxy for commands to be run locally
├── README.md
├── requirements-dev.txt  - Dev dependencies for the project
├── requirements.txt      - True dependencies for the project
└── tests
    └── unit              - Isolated tests
        └── test_dummy.py
```
