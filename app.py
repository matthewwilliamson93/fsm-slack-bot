"""
A slack bot for spreading the word of the Flying Spaghetti Monster.
"""

import re
import os

from slack_bolt import App


app = App(
    token=os.environ.get("SLACK_BOT_TOKEN"),
    signing_secret=os.environ.get("SLACK_SIGNING_SECRET"),
)


@app.message(re.compile("(noodly appendage | flying spaghetti monster)"))
def greet(message, say) -> None:
    """
    A listener to greet our welcoming flock.

    Args:
        message: A message from slack.
        say: A function to call back over with.
    """
    print("message: %s", message)
    lure = "Have you heard about or lord and savior the Flying Spaghetti Monster?"
    reel = (
        "To learn more come to: https://en.wikipedia.org/wiki/Flying_Spaghetti_Monster"
    )
    # pylint: disable=line-too-long
    hooked = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/Touched_by_His_Noodly_Appendage_HD.jpg/320px-Touched_by_His_Noodly_Appendage_HD.jpg"
    title = "Touched by His Noodly Appendage"

    say(
        blocks=[
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": lure,
                },
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": reel,
                },
            },
            {
                "type": "image",
                "image_url": hooked,
                "alt_text": title,
                "title": {
                    "type": "plain_text",
                    "text": title,
                },
            },
            {
                "type": "actions",
                "elements": [
                    {
                        "type": "button",
                        "text": {
                            "type": "plain_text",
                            "emoji": True,
                            "text": "I believe",
                        },
                        "style": "primary",
                        "value": "0",
                    },
                    {
                        "type": "button",
                        "text": {
                            "type": "plain_text",
                            "emoji": True,
                            "text": "I'm sorry what?",
                        },
                        "style": "danger",
                        "value": "1",
                    },
                ],
            },
        ]
    )


@app.action("button_click")
def action_button_click(message, ack, say) -> None:
    """
    A listener to respond to their prayers.

    Args:
        message: A message from slack.
        ack: A function to ack back with.
        say: A function to call back over with.
    """
    ack()
    print(message)
    user = message["user"]["name"]
    choice = "Yes" if message["actions"][0]["value"] == "0" else "No"
    say(f"<@{user}> said {choice}")


if __name__ == "__main__":
    app.start(port=int(os.environ.get("PORT", 3000)))
