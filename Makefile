PYTHON ?= python3.10
VENV    = .venv
TESTS   = tests
FILES  := $(shell find . -name "*.py" -not -path "./$(VENV)/*")

.PHONY: help
help:
	@echo "Usage:"
	@echo "    help:         Prints this screen"
	@echo "    venv:         Creates a virtual env"
	@echo "    install-deps: Installs dependencies"
	@echo "    check-fmt:    Checks the code for style issues"
	@echo "    fmt:          Make the formatting changes directly to the project"
	@echo "    lint:         Lints the code"
	@echo "    test:         Runs the local tests"
	@echo "    run:          Runs the slack bot"
	@echo "    clean:        Clean out temporaries"
	@echo ""

.PHONY: venv
venv:
	$(PYTHON) -m venv $(VENV)

.PHONY: install-deps
install-deps:
	$(PYTHON) -m pip install --no-cache-dir -r requirements-dev.txt -r requirements.txt

.PHONY: check-fmt
check-fmt:
	@echo "Format Checking"
	$(PYTHON) -m black --check .

.PHONY: fmt
fmt:
	@echo "Auto Formatting"
	$(PYTHON) -m black .

.PHONY: lint
lint:
	@echo "Python Type Checking"
	$(PYTHON) -m mypy --ignore-missing-imports $(FILES)
	@echo "Python Linting"
	$(PYTHON) -m pylint $(FILES)

.PHONY: run
run:
	$(PYTHON) app.py

.PHONY: test
test:
	$(PYTHON) -m pytest -rP $(TESTS)

.PHONY:
clean:
	@echo "Removing temporary files"
	rm -rf target/ "*.pyc" "__pycache__" ".mypy_cache" ".pytest_cache"
